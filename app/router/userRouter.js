//Import thư viện expresss
const express = require("express");

//Import controller
const { getAllUser, createUser, getUserById, updateUserById, deleteUserById } = require("../controller/userController");

//Khai báo router
const userRouter = express.Router();

//Create New User
userRouter.post("/users",createUser);

//Get All User
userRouter.get("/users",getAllUser);

//Get User By Id 
userRouter.get("/users/:userId",getUserById);

//Update User By Id 
userRouter.put("/users/:userId",updateUserById);

//Delete User By Id 
userRouter.delete("/users/:userId",deleteUserById);




//Export thành module 
module.exports = {userRouter};