//Import thư viện expresss
const express = require("express");

//Import controller
const { createNewPrize, getAllPrize, getPrizeById, updatePrizeById, deletePrizeById } = require("../controller/prizeController");




//Khai báo router
const prizeRouter = express.Router();

//Create New prize
prizeRouter.post("/prizes",createNewPrize);

//Get Allprize
prizeRouter.get("/prizes",getAllPrize);

//Get prize By Id 
prizeRouter.get("/prizes/:prizeId",getPrizeById);

//Update prize By Id 
prizeRouter.put("/prizes/:prizeId",updatePrizeById);

//Delete prize By Id 
prizeRouter.delete("/prizes/:prizeId",deletePrizeById);




//Export thành module 
module.exports = {prizeRouter};