//Import thư viện expresss
const express = require("express");

//Import controller
const { createDiceHisories, getAllDiceHistories, getDiceHistoriesById, updateDiceHistoriesById, deleteDiceHistoriesById } = require("../controller/diceHistoryController");


//Khai báo router
const diceHistoryRouter = express.Router();

//Create New Dice History
diceHistoryRouter.post("/dice-histories",createDiceHisories);

//Get All Dice History
diceHistoryRouter.get("/dice-histories",getAllDiceHistories);

//Get DiceHistory By Id 
diceHistoryRouter.get("/dice-histories/:diceHistoryId",getDiceHistoriesById);

//Update DiceHistory By Id 
diceHistoryRouter.put("/dice-histories/:diceHistoryId",updateDiceHistoriesById);

//Delete DiceHistory By Id 
diceHistoryRouter.delete("/dice-histories/:diceHistoryId",deleteDiceHistoriesById);




//Export thành module 
module.exports = {diceHistoryRouter};