//Import thư viện expresss
const express = require("express");


//Import controller
const { createNewVoucherHistories, getAllVoucherHistories, getVoucherHistoriesByid, updateVoucherHistoriesById, deleteVoucherHistoriesById } = require("../controller/voucherHistoryController");

//Khai báo router
const voucherHistoryRouter = express.Router();

//Create New voucher histories
voucherHistoryRouter.post("/voucher-histories", createNewVoucherHistories);

//Get All voucher histories
voucherHistoryRouter.get("/voucher-histories",getAllVoucherHistories);

//Get voucher histories By Id 
voucherHistoryRouter.get("/voucvoucher-historieshers/:historyId",getVoucherHistoriesByid);

//Update voucher histories By Id 
voucherHistoryRouter.put("/voucher-histories/:historyId", updateVoucherHistoriesById);

//Delete voucher histories By Id 
voucherHistoryRouter.delete("/voucher-histories/:historyId", deleteVoucherHistoriesById);




//Export thành module 
module.exports = {voucherHistoryRouter};