//Import thư viện expresss
const express = require("express");

//Import controller
const { createNewVoucher, getAllVoucher, getVoucherByid, updateVoucherById, deleteVoucherById,  } = require("../controller/voucherController");

//Khai báo router
const voucherRouter = express.Router();

//Create New voucher
voucherRouter.post("/vouchers",createNewVoucher);

//Get All voucher
voucherRouter.get("/vouchers",getAllVoucher);

//Get voucher By Id 
voucherRouter.get("/vouchers/:voucherId",getVoucherByid);

//Update voucher By Id 
voucherRouter.put("/vouchers/:voucherId",updateVoucherById);

//Delete voucher By Id 
voucherRouter.delete("/vouchers/:voucherId",deleteVoucherById);




//Export thành module 
module.exports = {voucherRouter};