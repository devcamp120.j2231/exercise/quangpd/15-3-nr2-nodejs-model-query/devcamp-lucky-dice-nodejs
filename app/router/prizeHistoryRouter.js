//Import thư viện expresss
const express = require("express");
const { updateDiceHistoriesById, deleteDiceHistoriesById } = require("../controller/diceHistoryController");


//Import controller
const { createNewPrizeHistories, getAllPrizeHistories, getPrizeHistoriesById,  } = require("../controller/prizeHistoryController");



//Khai báo router
const prizeHistoryRouter = express.Router();

//Create New prize histoires
prizeHistoryRouter.post("/prize-histories",createNewPrizeHistories);

//Get All prize histoires
prizeHistoryRouter.get("/prize-histories",getAllPrizeHistories);

//Get prize histoires By Id 
prizeHistoryRouter.get("/prize-histories/:historyId",getPrizeHistoriesById);

//Update prize histoires By Id 
prizeHistoryRouter.put("/prize-histories/:historyId",updateDiceHistoriesById);

//Delete prize histoires By Id 
prizeHistoryRouter.delete("/prize-histories/:historyId", deleteDiceHistoriesById);




//Export thành module 
module.exports = {prizeHistoryRouter};