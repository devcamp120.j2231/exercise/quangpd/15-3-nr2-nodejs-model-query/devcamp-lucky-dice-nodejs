//Import thư viện mongoose
const mongoose = require("mongoose");

//Import prizeModel 
const prizeHistoryModel  = require("../model/prizeHistoryModel");

//Create New Prizes histories
const createNewPrizeHistories = (req,res) =>{
    //B1 : thu thập dữ liệu
    var body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!body.user){
        return res.status(400).json({
            message :`Bad request - user is required !`
        })
    }

    if(!body.prize){
        return res.status(400).json({
            message :`Bad request - prize is required !`
        })
    }

    //B3 : Xử lý dữ liệu
    let newPrizeHistories = new prizeHistoryModel({
        _id : mongoose.Types.ObjectId(),
        user : body.user,
        prize : body.prize
    });

    prizeHistoryModel.create(newPrizeHistories,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Create New Prize Histories Successfully !`,
                PrizeHistories : data
            })
        }
    })
}

//Get All Prizes histories
const getAllPrizeHistories = (req,res) =>{
    //B1 : thu thập dữ liệu
    let user = req.query.user;
    
    //Tạo ra đối tượng lọc
    let condition = {};
    if(user){
        condition.user = user;
    }

    console.log(condition);
    //B2 : Kiểm tra dữ liệu
    //B3 : Xử lý và trả về dữ liệu
    prizeHistoryModel.find(condition,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(200).json({
                message :`Get All Prize Histories Successfully !`,
                PrizesHistories : data
            })
        }
    })
}

//Get Prizes Id histories
const getPrizeHistoriesById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let historyId = req.params.historyId;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(historyId)){
        return res.status(400).json({
            message : `Bad request - historyId is not valid !`
        })
    }

    //B3 : Xử lý và trả về kết quả 
    prizeHistoryModel.findById(historyId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message : `Load Prize Histories By Id Successfully !`,
                PrizesHistories : data
            })
        }
    })
}

//Update Prizes Id histories
const updatePrizeHistoriesById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let historyId = req.params.historyId;
    var body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(historyId)){
        return res.status(400).json({
            message :`Bad request - historyId is not valid !`
        })
    }

    if(!body.user){
        return res.status(400).json({
            message :`Bad request - user is required !`
        })
    }

    if(!body.prize){
        return res.status(400).json({
            message :`Bad request - prize is required !`
        })
    }
   

    //B3: Xử lý và thu thập dữ liệu
    let updatePrizeHistories = new prizeHistoryModel({
        user : body.user,
        prize : body.prize
    });

    prizeHistoryModel.findByIdAndUpdate(historyId,updatePrizeHistories,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :` Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Update Prize Histories By Id Successfully !`,
                PrizesHistories : data
            })
        }
    })
}

//Delete Prizes By Id histories
const deletePrizeHistoriesById = (req,res) =>{
    //B1 : thu thập dư liệu
    let historyId = req.params.historyId;

    //B2 : kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(historyId)){
        return res.status(400).json({
            message : `Bad request - historyId is not valid `
        })
    }

    //B3 : Xử lý và trả về dữ liệu
    prizeHistoryModel.findByIdAndDelete(historyId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(204).send()
        }
    })
}
//Export thành module 
module.exports = { createNewPrizeHistories , getAllPrizeHistories , getPrizeHistoriesById , updatePrizeHistoriesById , deletePrizeHistoriesById };