//Import thư viện mongoose
const mongoose = require("mongoose");

//Import voucherModel
const voucherModel = require("../model/voucherModel"); 


//Create New Voucher
const createNewVoucher = (req,res) =>{
    //B1 : thu thập dữ liệu
    var body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!body.code){
        return res.status(400).json({
            message :`Bad request - code is required !`
        })
    }

    if(!body.discount){
        return res.status(400).json({
            message :`Bad request - discount is required !`
        })
    }

    if(!Number(body.discount)){
        return res.status(400).json({
            message :`Bad request - discount is a number !`
        })
    }

    //B3 : Xử lý dữ liệu
    let newVoucher = new voucherModel({
        _id : mongoose.Types.ObjectId(),
        code : body.code,
        discount : body.discount,
        note : body.note
    });

    voucherModel.create(newVoucher,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Create New Voucher Successfully !`,
                Voucher : data
            })
        }
    })
}

//Get All Voucher
const getAllVoucher = (req,res) =>{
    //B1 : thu thập dữ liệu
    //B2 : Kiểm tra dữ liệu
    //B3 : Xử lý và trả về dữ liệu
    voucherModel.find((err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(200).json({
                message :`Get All Voucher Successfully !`,
                Vouchers : data
            })
        }
    })
}

//Get Voucher By Id 
const getVoucherByid = (req,res) =>{
    //B1 : thu thập dữ liệu
    let voucherId = req.params.voucherId;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)){
        return res.status(400).json({
            message : `Bad request - voucherId is not valid !`
        })
    }

    //B3 : Xử lý và trả về kết quả 
    voucherModel.findById(voucherId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message : `Load Voucher By Id Successfully !`,
                Vouchers : data
            })
        }
    })
}

//Update Prizes Id
const updateVoucherById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let voucherId = req.params.voucherId;
    var body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)){
        return res.status(400).json({
            message :`Bad request - voucherId is not valid !`
        })
    }

    if(!body.code){
        return res.status(400).json({
            message :`Bad request - code is required !`
        })
    }

    if(!body.discount){
        return res.status(400).json({
            message :`Bad request - discount is required !`
        })
    }

    if(!Number(body.discount)){
        return res.status(400).json({
            message :`Bad request - discount is a number !`
        })
    }

    //B3: Xử lý và thu thập dữ liệu
    let updateVoucher = new voucherModel({
       name : body.name,
       description : body.description
    });

    voucherModel.findByIdAndUpdate(voucherId,updateVoucher,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :` Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Update Voucher By Id Successfully !`,
                Voucher : data
            })
        }
    })
}

//Delete Prizes By Id 
const deleteVoucherById = (req,res) =>{
    //B1 : thu thập dư liệu
    let voucherId = req.params.voucherId;

    //B2 : kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)){
        return res.status(400).json({
            message : `Bad request - voucherId is not valid `
        })
    }

    //B3 : Xử lý và trả về dữ liệu
    voucherModel.findByIdAndDelete(voucherId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(204).send()
        }
    })
}
//Export thành module 
module.exports = { createNewVoucher , getAllVoucher , getVoucherByid ,updateVoucherById , deleteVoucherById};