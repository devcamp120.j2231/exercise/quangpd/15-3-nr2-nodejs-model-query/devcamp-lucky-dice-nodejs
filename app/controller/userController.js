//Import thư viện mongoose
const mongoose = require("mongoose");

//Import userModel 
const userModel = require("../model/userModel");

//Create New User
const createUser = (req,res) =>{
    //B1 : thu thập dữ liệu
    var body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!body.username){
        return res.status(400).json({
            message :`Bad request - username is required !`
        })
    }

    if(!body.firstname){
        return res.status(400).json({
            message :`Bad request - firstname is required !`
        })
    }

    if(!body.lastname){
        return res.status(400).json({
            message :`Bad request - lastname is required !`
        })
    }

    //B3 : Xử lý dữ liệu
    let newUser = new userModel({
        _id : mongoose.Types.ObjectId(),
        username : body.username,
        firstname : body.firstname,
        lastname : body.lastname
    });

    userModel.create(newUser,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Create New User Successfully !`,
                user : data
            })
        }
    })
}

//Get All User
const getAllUser = (req,res) =>{
    //B1 : thu thập dữ liệu
    //B2 : Kiểm tra dữ liệu
    //B3 : Xử lý và trả về dữ liệu
    userModel.find((err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(200).json({
                message :`Get All User Successfully !`,
                users : data
            })
        }
    })
}

//Get User Id 
const getUserById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let userId = req.params.userId;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(400).json({
            message : `Bad request - UserId is not valid !`
        })
    }

    //B3 : Xử lý và trả về kết quả 
    userModel.findById(userId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message : `Load User By User Id Successfully !`,
                user : data
            })
        }
    })
}

//Update User Id
const updateUserById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let userId = req.params.userId;
    var body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(400).json({
            message :`Bad request - userId is not valid !`
        })
    }

    if(!body.username){
        return res.status(400).json({
            message :`Bad request - username is required !`
        })
    }

    if(!body.firstname){
        return res.status(400).json({
            message :`Bad request - firstname is required !`
        })
    }

    if(!body.lastname){
        return res.status(400).json({
            message :`Bad request - lastname is required !`
        })
    }

    //B3: Xử lý và thu thập dữ liệu
    let updateUser = new userModel({
        username : body.username,
        firstname : body.firstname,
        lastname : body.lastname
    });

    userModel.findByIdAndUpdate(userId,updateUser,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :` Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Update User By Id Successfully !`,
                users : data
            })
        }
    })
}

//Delete User By Id 
const deleteUserById = (req,res) =>{
    //B1 : thu thập dư liệu
    let userId = req.params.userId;

    //B2 : kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(userId)){
        return res.status(400).json({
            message : `Bad request - userId is not valid `
        })
    }

    //B3 : Xử lý và trả về dữ liệu
    userModel.findByIdAndDelete(userId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(204).send()
        }
    })
}
//Export thành module
module.exports = {getAllUser, createUser , getUserById ,updateUserById,deleteUserById};