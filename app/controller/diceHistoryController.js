//Import thư viện mongoose
const mongoose = require("mongoose");

//Import diceHistory model 
const diceHistoryModel = require("../model/diceHistoryModel");

//Create New Dice Histories
const createDiceHisories = (req,res) =>{
    //B1 : thu thập dữ liệu
    var body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!body.user){
        return res.status(400).json({
            message :`Bad request - user is required !`
        })
    }

    //B3 : Xử lý dữ liệu
    let newDiceHistories = new diceHistoryModel({
        _id : mongoose.Types.ObjectId(),
        user : body.user
    });

    diceHistoryModel.create(newDiceHistories,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Create New DiceHistory Successfully !`,
                DiceHistories : data
            })
        }
    })
}

//Get All Dice Histories
const getAllDiceHistories = (req,res) =>{
    //B1 : thu thập dữ liệu
    let user = req.query.user;

    //tạo đỗi tượng lọc
    let condition = {};
    if(user){
        condition.user = user;
    }

    console.log(condition);

    //B2 : Kiểm tra dữ liệu
    //B3 : Xử lý và trả về dữ liệu
    diceHistoryModel.find(condition,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(200).json({
                message :`Get All Dice Histories Successfully !`,
                users : data
            })
        }
    })
}

//Get DiceHistories By Id 
const getDiceHistoriesById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let diceHistoryId = req.params.diceHistoryId;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(diceHistoryId)){
        return res.status(400).json({
            message : `Bad request - diceHistoryId is not valid !`
        })
    }

    //B3 : Xử lý và trả về kết quả 
    diceHistoryModel.findById(diceHistoryId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message : `Load diceHistory By Id Successfully !`,
                user : data
            })
        }
    })
}

//Update DiceHistories By Id
const updateDiceHistoriesById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let udiceHistoryIdserId = req.params.diceHistoryId;
    var body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(diceHistoryId)){
        return res.status(400).json({
            message :`Bad request - diceHistoryId is not valid !`
        })
    }

    if(!body.user){
        return res.status(400).json({
            message :`Bad request - user is required !`
        })
    }

    //B3: Xử lý và thu thập dữ liệu
    let updateDiceHistories = new userModel({
        user : body.user
    });

    diceHistoryModel.findByIdAndUpdate(diceHistoryId,updateDiceHistories,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :` Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Update DiceHistories By Id Successfully !`,
                users : data
            })
        }
    })
}

//Delete DiceHistories By Id 
const deleteDiceHistoriesById = (req,res) =>{
    //B1 : thu thập dư liệu
    let diceHistoryId = req.params.diceHistoryId;

    //B2 : kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(diceHistoryId)){
        return res.status(400).json({
            message : `Bad request - diceHistoryId is not valid `
        })
    }

    //B3 : Xử lý và trả về dữ liệu
    diceHistoryModel.findByIdAndDelete(diceHistoryId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(204).send()
        }
    })
}
//Export thành module
module.exports = { createDiceHisories , getAllDiceHistories , getDiceHistoriesById , updateDiceHistoriesById , deleteDiceHistoriesById};