//Import thư viện mongoose
const mongoose = require("mongoose");

//Import voucherModel
const voucherHistoryModel = require("../model/voucherHistoryModel"); 


//Create New Voucher Histories
const createNewVoucherHistories = (req,res) =>{
    //B1 : thu thập dữ liệu
    var body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!body.user){
        return res.status(400).json({
            message :`Bad request - user is required !`
        })
    }

    if(!body.voucher){
        return res.status(400).json({
            message :`Bad request - voucher is required !`
        })
    }


    //B3 : Xử lý dữ liệu
    let newVoucherHistories = new voucherHistoryModel({
        _id : mongoose.Types.ObjectId(),
        user : body.user,
        voucher : body.voucher
    });

    voucherHistoryModel.create(newVoucherHistories,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Create New Voucher Histories Successfully !`,
                VoucherHistories : data
            })
        }
    })
}

//Get All Voucher Histories
const getAllVoucherHistories = (req,res) =>{
    //B1 : thu thập dữ liệu
    let user = req.query.user;
    //Tạo mới đối tượng lọc
    let condition = {};

    if(user){
        condition.user = user;
    }

    console.log(condition);
    //B2 : Kiểm tra dữ liệu
    //B3 : Xử lý và trả về dữ liệu
    voucherHistoryModel.find(condition,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(200).json({
                message :`Get All VoucherHistories Successfully !`,
                VouchersHistories : data
            })
        }
    })
}

//Get Voucher Histories By Id 
const getVoucherHistoriesByid = (req,res) =>{
    //B1 : thu thập dữ liệu
    let historyId = req.params.historyId;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(historyId)){
        return res.status(400).json({
            message : `Bad request - historyId is not valid !`
        })
    }

    //B3 : Xử lý và trả về kết quả 
    voucherHistoryModel.findById(historyId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message : `Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message : `Load Voucher Histories By Id Successfully !`,
                VouchersHistories : data
            })
        }
    })
}

//Update Voucher Histories Id
const updateVoucherHistoriesById = (req,res) =>{
    //B1 : thu thập dữ liệu
    let historyId = req.params.historyId;
    var body = req.body;

    //B2 : Kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(historyId)){
        return res.status(400).json({
            message :`Bad request - historyId is not valid !`
        })
    }

    if(!body.user){
        return res.status(400).json({
            message :`Bad request - user is required !`
        })
    }

    if(!body.voucher){
        return res.status(400).json({
            message :`Bad request - voucher is required !`
        })
    }

    //B3: Xử lý và thu thập dữ liệu
    let updateVoucherHistories = new voucherHistoryModel({
        user : body.user,
        voucher : body.voucher
    });

    voucherHistoryModel.findByIdAndUpdate(voucherId,updateVoucherHistories,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :` Internal server error : ${err.message}`
            })
        }else{
            return res.status(201).json({
                message :`Update Voucher Histories By Id Successfully !`,
                VoucherHistories : data
            })
        }
    })
}

//Delete Voucher Histories By Id 
const deleteVoucherHistoriesById = (req,res) =>{
    //B1 : thu thập dư liệu
    let historyId = req.params.voucherId;

    //B2 : kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(historyId)){
        return res.status(400).json({
            message : `Bad request - historyId is not valid `
        })
    }

    //B3 : Xử lý và trả về dữ liệu
    voucherHistoryModel.findByIdAndDelete(historyId,(err,data) =>{
        if(err){
            return res.status(500).json({
                message :`Internal server error : ${err.message}`
            })
        }else{
            return res.status(204).send()
        }
    })
}
//Export thành module 
module.exports = { createNewVoucherHistories , getAllVoucherHistories , getVoucherHistoriesByid , updateVoucherHistoriesById , deleteVoucherHistoriesById };