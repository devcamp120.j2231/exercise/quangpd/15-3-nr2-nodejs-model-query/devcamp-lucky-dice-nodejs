//Import thư viện mongoose
const mongoose  = require("mongoose");

//Import thư viện Schema của mongoose
const Schema = mongoose.Schema;

const voucherHistorySchema = new Schema({
    _id : mongoose.Types.ObjectId,
    user : 
        {
        type : mongoose.Types.ObjectId,
        ref : 'User',
        required : true
        }
    ,
    voucher : 
        {
        type : mongoose.Types.ObjectId,
        ref : 'voucher',
        required : true
        }
    ,
    createdAt : {
        type :Date,
        default : Date.now()
    },
    updatedAt : {
        type : Date,
        default : Date.now()
    }
})

module.exports = mongoose.model("voucherHistory",voucherHistorySchema);